// This is controller used for sending the request collected for the source.

var service = require('../service/userservice'); // import user service and initialize to service variable.

// Add User Method
module.exports.addUser = function (req, res) {
    // passing request to dao layer
    service.addUser(req, function (user) {
        res.status(200); // status for the response
        res.json(user); // sending response via same source we collected reuest
    })
}