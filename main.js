var express = require('express');
var mongoose = require('mongoose');
var router = require("./routes/route");
const bodyParser = require('body-parser');

var port = process.env.PORT || 8080; // Define Port No.
var database = require('./config/database');  //Load Database Config

var app = express();

app.use(bodyParser.json({ limit: '50mb' }));  // use to parse the request body to json

// use to handle CORS

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-XSRF-TOKEN");
    next();
});

// call routing file
app.use("/api", router);

// options for mongoose DB

const options = {
    autoIndex: false, // Don't build indexes
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
};

// connect to mongoose DB

mongoose.connect(database.localUrl, options).then(
    () => {
        // console.log(mongoose.connection.readyState); // print mongo db connecting state

        var server = app.listen(port, function () {
            console.log('Express server listening on port ' + server.address().port);
        });
        /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
},
    err => { /** handle initial connection error */ }
);

