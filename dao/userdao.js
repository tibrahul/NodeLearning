// This is dao layer, where we will perform simple CRUD

var model = require('../modals/user'); // import model which is which we have to do CRUD.

//Dao Layer for saving a data
module.exports.addUser = function (req, callback) {
    var userData = req.body; // collecting the body data from the request
    var usermodel = new model(userData); // initializing model object

    // save is used for saving or create a document in collection
    usermodel.save(function (err, userData) {
        if (err) {
            callback(err); // send the error, if we collect during any transaction
        } else {
            callback(userData); // send the response to service via callback
        }
    });
}