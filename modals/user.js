var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');

var user = new mongoose.Schema({
   id: {type : Number},
   email: {type : String, required: true},
   password: {type : String, required: true},
   name: {type : String, required: true},
   created_date:{type : Date ,default: Date.now()},
   updated_date:{type : Date ,default: null}
},{
    versionKey: false // You should be aware of the outcome after set to false
});

autoIncrement.initialize(mongoose);
user.plugin(autoIncrement.plugin,{ model: 'user', startAt: 1 });
module.exports = mongoose.model('user', user);