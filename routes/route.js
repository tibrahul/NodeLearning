var express = require("express");
var routers = require("./routers");

var api = express.Router();

api.use("/user", routers.UserRouter);

module.exports = api;