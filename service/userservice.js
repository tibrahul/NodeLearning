// This is service part which collect request from controller.
// if needed, apply business logic and collected required data for CRUD operation.
// pass the required data to dao layer

var dao = require('../dao/userdao');  // import user dao and initialize to dao variable.

// Add user service layer
module.exports.addUser = function (req, callback) {
    // passing request to dao layer
    dao.addUser(req, function (user) {
        callback(user); // send the response to controller via callback
    })
}